module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './component/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      'roboto': 'Roboto'
    },
    screens: {
      'sm': '320px',
      // => @media (min-width: 640px) { ... }

      'md': '500px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
    },
    extend: {
      colors: {
        highlight: '#df3751',
        active: '#4ACFFF',
        'active-lighter': '#42C1EE',
        'not-active': '#888888',
        primary: '#3DA6D0',
        dark: '#4A4A4A',
        'light-grey': '#9B9B9B',
        'lighter-grey': '#949494',
        'line-grey' : '#f2f2f2',
        darker: '#1F2320',
        'dark-blue': '#113C6D',
        'darker-blue': '#317C9E',
        'light-green': '#49C2BA',
        'darkest-blue': '#113C6D'
      },
      spacing: {
        sm: '8px',
        md: '16px',
        lg: '24px',
        xl: '48px',
        header: '618px'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
