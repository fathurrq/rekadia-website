import Image from 'next/image'
import Carousel from 'react-elastic-carousel'
import logo0 from '/public/client-logo/0.png'
import logo1 from '/public/client-logo/1.png'
import logo2 from '/public/client-logo/2.png'
import logo3 from '/public/client-logo/3.jpg'
import logo4 from '/public/client-logo/4.png'
import logo5 from '/public/client-logo/5.jpg'
import logo6 from '/public/client-logo/6.png'
import logo7 from '/public/client-logo/7.svg'
import logo8 from '/public/client-logo/8.png'
import logo9 from '/public/client-logo/9.png'
import logo10 from '/public/client-logo/10.jpg'
import logo11 from '/public/client-logo/11.png'
import logo12 from '/public/client-logo/12.png'
import logo13 from '/public/client-logo/13.jpg'
import logo14 from '/public/client-logo/14.png'

const logo = [logo0, logo1, logo2, logo3, logo4, logo5, logo6, logo8, logo9, logo10, logo11, logo12, logo13, logo14]
// const logo = ['/client-logo/0.png', '/client-logo/1.png',
//     '/client-logo/2.png', '/client-logo/3.jpg', '/client-logo/4.png',
//     '/client-logo/5.jpg', '/client-logo/6.png', '/client-logo/7.svg',
//     '/client-logo/8.png', '/client-logo/9.png', '/client-logo/10.jpg',
//     '/client-logo/11.png', '/client-logo/12.png', '/client-logo/13.jpg', '/client-logo/14.png']

const ImageSlider = () => {
    const breakPoints = [
        { width: 1, itemsToShow: 3, pagination: false },
        { width: 550, itemsToShow: 5, pagination: false },
        { width: 850, itemsToShow: 5, pagination: false },
        { width: 1150, itemsToShow: 5, pagination: false },
        { width: 1450, itemsToShow: 5, pagination: false },
        { width: 1750, itemsToShow: 5, pagination: false },
    ]
    return (
        <Carousel breakPoints={breakPoints}>
            {
                logo.map((d, i) => (
                    <div className="lg:h-36 xl:h-48 flex justify-center items-center" key={i}>
                        <div className={d.height < 500 ? "lg:p-4 xl:p-8" : "lg:p-4 xl:p-8 w-3/4"}>
                            <Image  src={d} />
                        </div>
                    </div>
                ))
            }

            {/* <Image    src={logo1} width={180} height={60}/>
            <Image    src={logo2} width={180} height={60}/>
            <Image    src={logo3} width={180} height={60}/>
            <Image    src={logo4} width={180} height={60}/>
            <Image    src={logo5} width={180} height={60}/>
            <Image    src={logo6} width={180} height={60}/>
            <Image    src={logo7} width={180} height={60}/>
            <Image    src={logo8} width={180} height={60}/>
            <Image    src={logo9} width={120} height={60}/>
            <Image    src={logo10} width={150} height={60}/>
            <Image    src={logo11} width={180} height={60}/>
            <Image    src={logo12} width={180} height={60}/>
            <Image    src={logo13} width={180} height={60}/>
            <Image    src={logo14} width={150} height={60}/> */}
        </Carousel>
    )
}
export default ImageSlider