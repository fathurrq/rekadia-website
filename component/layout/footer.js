import Image from 'next/image'
import logoBlack from '/public/logo-black.png'
import instagram from '/public/instagram.png'
import linkedin from '/public/linkedin.png'
import message from '/public/message.svg'
import Link from 'next/link'
import pin from '/public/pin.svg'
import phone from '/public/wa.svg'
const Footer = () => {
    return (
        <div className="flex flex-col justify-center h-80 p-8">
            <div className="mb-4">
                <Image  src={logoBlack} />
            </div>
            <h2 className="text-light-grey font-bold mb-4 text-lg">PT. Rekadia Solusi Teknologi</h2>
            <div className="grid grid-cols-3">
                <div>
                    <div>
                        <div className="flex items-center">
                            <Image  src={pin} />
                            <h2 className="text-light-grey text-sm mb-0">HEAD QUARTER</h2>
                        </div>
                        <div className="flex flex-col mb-3 ml-9">
                            <h2 className="text-light-grey text-xs">Metropolitan Tower, Lv. 13 Jl. R.A. Kartini Kav. 14,<br /> TB Simatupang, Jakarta Selatan</h2>
                        </div>
                    </div>
                    <div>
                        <div className="flex items-center">
                            <Image  src={pin} />
                            <h2 className="text-light-grey text-sm mb-0">WORKSHOP</h2>
                        </div>
                        <div className="flex flex-col mb-3 ml-9">
                            <h2 className="text-light-grey text-xs">
                                Jl. Pasir Salam Raya I/9, BANDUNG, Indonesia
                            </h2>
                        </div>
                    </div>
                </div>
                <div className="flex justify-center">
                    <div className="flex flex-col">
                        <h2 className="text-light-grey font-bold mb-4" style={{ fontSize: 18 }}>Connect With Us</h2>
                        <div className="flex mb-4">
                            <Image  src={phone} />
                            <h2 className="text-light-grey text-xs ml-2 mb-0">(021) 2955 2653</h2>
                        </div>
                        <div className="flex mb-4 items-center">
                            <Image  src={message} />
                            <h2 className="text-light-grey text-xs ml-2 mb-0">contact@rekadia.co.id</h2>
                        </div>
                        <div className="flex">
                            <Link href={'http://instagram.com/rekadia'} passHref>
                                <div className="mr-2.5">
                                    <Image  src={instagram} height={28} width={28} />
                                </div>
                            </Link>
                            <Link href={'https://www.linkedin.com/company/rekadia/'} passHref>
                                <div>
                                    <Image  src={linkedin} height={28} width={28} />
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="flex justify-center">
                    <div className="flex flex-col">
                        <a className="text-primary font-bold mb-2 text-lg" href={'/about-us'}>About Us</a>
                        <a className="text-primary font-bold mb-2 text-lg">Solution</a>
                        <a className="text-primary font-bold text-lg" href={'/join-us'}>Career</a>

                    </div>
                </div>

            </div>
        </div>
    )
}
export default Footer