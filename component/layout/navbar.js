import logo from '/public/logo.png'
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'
import { useRouter } from 'next/router'

const menu = [
    {
        name: "About Us",
        key: 1,
        link: "/about-us",
    },
    {
        name: "Solution",
        key: 2,
        link: "solution",
        subMenu: [{
            name: 'Production Monitoring Software',
            key: 1,
            link: '/solution/product-monitoring'
        },
        {
            name: 'Maintenance & Inspection Software',
            key: 2,
            link: '/solution/maintenance'
        },
        {
            name: 'HSSE Software',
            key: 3,
            link: '/solution/hsse'
        },
        {
            name: 'Other',
            key: 4,
            link: '/solution/other'
        }
        ]
    },
    {
        name: "Contact Us",
        key: 3,
        link: "/contact-us",
    },
    {
        name: "Join With Us",
        key: 4,
        link: "/join-us",
    },
]
const Navbar = () => {
    const router = useRouter()
    const [dropDown, setDropDown] = useState(false)
    return (
        <div className="absolute top-0 flex justify-between w-full items-end border-b border-gray-400 sm:bg-dark-blue xl:bg-transparent lg:bg-transparent" style={{height:122}}>
            <div className="p-8 pb-4">
                <Link href={'/'} passHref>
                    <Image    src={logo} />
                </Link>
            </div>
            <div className=" pb-4 sm:hidden xl:flex lg:flex">
                {
                    menu.map((d) => {
                        if (d.subMenu) {
                            return <div className="relative">
                                <a className={router.pathname.split('/')[1] === d.link ? "mx-4 text-active font-roboto-medium lg:text-sm xl:text-lg" : "mx-4  text-line-grey lg:text-sm xl:text-lg"} onClick={() => {
                                    setDropDown(!dropDown)
                                }}>{d.name}</a>
                                {
                                    dropDown ?
                                        <div className="origin-top-right absolute left-0 top-9 mt-2 w-64 shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-light-grey divide- focus:outline-none background-nav">
                                            {
                                                d.subMenu.map((d) => (
                                                    <div>
                                                        <Link href={d.link} passHref>
                                                            <a href="#" className={router.pathname === d.link ? "text-active block px-4 py-1.5 text-sm font-roboto-medium" : "text-light-grey block px-4 py-1.5 text-sm font-roboto-medium"}>{d.name}</a>
                                                        </Link>
                                                    </div>
                                                ))
                                            }
                                        </div> : null
                                }
                            </div>
                        }
                        else {
                            return <div>
                                <Link href={d.link} passHref>
                                    <a style={d.key === 4 ? { marginRight: 32 } : undefined} className={router.pathname === d.link ? "mx-4 text-active font-roboto-medium lg:text-sm xl:text-lg" : "mx-4  text-line-grey lg:text-sm xl:text-lg font-roboto-medium"}>{d.name}</a>
                                </Link>
                            </div>
                        }
                    })
                }

            </div>

        </div>
    )
}
export default Navbar