import Image from 'next/image'
import Footer from '../component/layout/footer'
import Navbar from '../component/layout/navbar'
import Link from 'next/link'
import download from '/public/icon-download.png'
import content from '/public/about-us-2.png'
import earthLeft from '/public/join-earth-left.png'
import earthRight from '/public/join-earth-right.png'
import { Carousel } from 'antd';


const job = [{
    name: 'Junior Programmer',
    link: '/'
}, {
    name: 'Junior Business Development',
    link: '/'
}
]
export default function JoinUs() {
    return (
        <div>
            <div style={{
                backgroundImage: `url('/header-join.png')`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                height: 595
            }
            }
                className="relative w-full"
            >
                <Navbar />
                <div className="absolute left-7 lg:bottom-32">
                    {/* <h2 className="text-white lg:text-xl xl:text-2xl mb-6 font-sans">We Are Here for Your Business</h2> */}
                    <h1 className="text-white mb-5 font-robotoslab " style={{ width: 875, fontSize: 42, lineHeight: '3.5rem' }}>
                        We are a vibrant group of people with a zealous commitment toward the mission and values of our company.
                    </h1>
                </div>
            </div>
            <div className="grid grid-cols-2" style={{ height: 400 }}>
                <div className="gradient-bg flex flex-col items-center justify-center">
                    <p className=" text-white text-lg font-bold text-center mb-9 xl:text-2xl">Our Mission</p>
                    <p className="text-white text-4xl text-center" style={{ width: 500 }}>Provides effective solutions to empower organizations</p>
                </div>
                <div className=" flex items-center justify-center">
                    <p className="lg:text-4xl xl:text-5xl text-center text-light-grey w-80 m-0 pt-4 " style={{ width: 500 }}>“Our mission defines us and gives higher purpose in everthing that we do.”</p>
                </div>
            </div>
            <div className="grid grid-cols-2">
                <div className="flex flex-col items-center justify-center">
                    <p className="lg:text-4xl xl:text-5xl text-center text-light-grey m-0 pt-4 " style={{ width: 500 }}>“Our values provide clear guidelines in our actions. We know what to do and what is expected of our other team members.”</p>
                </div>
                <div className="bg-light-green flex items-center justify-center p-4" style={{ height: "400px" }}>
                    <Image src={content} />
                </div>
            </div>
            <div className="grid grid-cols-2" style={{ height: 300 }}>
                <div style={{
                    backgroundImage: `url('/join1.jpg')`,
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "100% 300px",
                    height: 300
                }}>
                </div>
                <div className=" items-center justify-center bg-darkest-blue">
                    {/* <h2 className="text-2xl font-bold text-white text-center mt-24 mb-6 pb-0">Rekadian Program</h2> */}
                    <div className="mt-24">
                    <Carousel autoplay={true} >
                        <div className="flex items-center justify-center bg-darkest-blue text-center">
                            <h2 className="text-4xl font-bold text-white h-24">Upskill Training</h2>
                        </div>
                        <div className="flex items-center justify-center bg-darkest-blue text-center">
                            <h2 className="text-4xl font-bold text-white h-24">Retrospective Meeting</h2>
                        </div>
                        <div className="flex items-center justify-center bg-darkest-blue text-center">
                            <h2 className="text-4xl font-bold text-white h-24">One-on-one Coaching</h2>
                        </div>
                        <div className="flex items-center justify-center bg-darkest-blue text-center">
                            <h2 className="text-4xl font-bold text-white h-24">Performance-based Benefit</h2>
                        </div>
                        <div className="flex items-center justify-center bg-darkest-blue text-center h-24">
                            <h2 className="text-4xl font-bold text-white">Company Outing</h2>
                        </div>
                    </Carousel>
                    </div>
                </div>
            </div>
            <div style={{ padding: 30, height: 345 }} >
                <div className="w-full flex flex-col items-center border border-light-green h-72 relative">
                    <div style={{ position: "absolute", left: "-70px", top: -50,zIndex: -10 }}>
                        <Image src={earthLeft} />
                    </div>
                    <h2 className="text-2xl text-dark font-bold text-center mt-10 mb-6">We LOVE earth!</h2>
                    <p className="text-light-grey font-medium text-center">
                        We actively take part in the sustainability and preservation
                        of our beloved earth. <br />
                        Disposable plastic is <b>banned</b> in our office! <br />
                        We provide reusable and foldable bag for each of our member. We <b>separate</b> <br />
                        plastic waste and paper for recycling. <br />
                        The income gained from recycling is used for social cause.
                    </p>
                    <div className="absolute" style={{right: -20, top: -50, zIndex: -10}}>
                        <Image src={earthRight} />
                    </div>
                </div>
            </div>
            <div className="p-12 w-full bg-darker-blue flex flex-col items-center relative z-50">
                <h2 className="text-center text-4xl text-white mb-12" style={{ width: 500 }}>
                    You have the opportunity to be part of Rekadian NOW!
                </h2>
                <div className="grid grid-cols-2 w-full">
                    <div className="py-5 pr-3">
                        <h2 className="text-white text-lg font-bold">Open Position</h2>
                        {
                            job.map((d) => (
                                <div className="flex w-full justify-between mb-4 items-center">
                                        <p className="text-white font-bold text-2xl m-0 p-0" style={{maxWidth: 280}}>{d.name}</p>
                                    <Link href={d.link}>
                                        <button className="p-4 text-white bg-primary font-bold text-base pb-3">Download Brochure</button>
                                    </Link>
                                </div>
                            ))
                        }
                    </div>
                    <div className="px-10 py-5">
                        <h2 className="text-white font-bold mb-2">Send your CV to</h2>
                        <p className="text-white font-bold text-2xl mb-0 ">contact@rekadia.co.id</p>
                        <p className="text-white">and we will reply within 2 business days.</p>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}
