import Image from 'next/image'
import Footer from '../../component/layout/footer'
import Navbar from '../../component/layout/navbar'
import imageMac from '/public/image_mockup.png'
import computerImage from '/public/image_maintenance.png'
import logo1 from '/public/maintenance-logo1.png'
import logo2 from '/public/maintenance-logo2.png'
import logo3 from '/public/maintenance-logo3.png'
import logo4 from '/public/maintenance-logo4.png'
import logo5 from '/public/maintenance-logo5.png'
import abstract from '/public/abstract-motive.png'
import Link from 'next/link'

export default function Maintenance() {
    return (
        <div>
            <div style={{
                backgroundImage: `url('/maintenance-content.jpg')`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                height: 500
            }
            }
                className="relative w-full"
            >
                <Navbar />
                <div className="absolute left-7 bottom-32">
                    <h1 className="text-white space-x-2 space-y-8  m-0 font-robotoslab" style={{ fontSize: 42 }}>
                        Maintenance & Inspection Software
                    </h1>
                    <h2 className="text-white lg:text-sm xl:text-md font-sans ">Empower field worker and maintain optimal facility operating condition</h2>
                </div>
            </div>
            <div className="w-full flex justify-center relative flex-col px-7 pt-9" style={{ marginBottom: 120 }}>
                <p className="w-3/5 text-sm text-light-grey">
                    Maintenance software aims to improve plant productivity by maximizing plant uptime and reducing unplanned downtime. Our software enables company to proactively ensuring the optimal operating condition of equipment through comprehensive maintenance schedule, recording of maintenance activities, and monitoring.
                    Inspection software aims to ensure the operation integrity of stationary equipment, i.e. pipe, vessel, and storage tank. The purpose of the software is to reduce the risk of leakage through continuous calculation of risk, smart inspection schedule, and recording of inspection reports.
                    Our team of seasoned experts also provides services for assessment and improvement of maintenance & inspection strategy based on international standard.
                </p>
                <div className="absolute right-0" style={{ bottom: -200 }}>
                    <Image src={computerImage} />
                </div>
            </div>
            <div className="grid grid-cols-2 gap-16 mb-10">
                <div>
                    <Image src={imageMac} />
                </div>

                <div>
                    <h2 className="mb-5 mt-10 text-dark text-4xl">Features</h2>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Enriched asset data</h4>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Maintenance and inspection schedule</h4>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Maintenance and inspection report</h4>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Equipment failure and leakage report</h4>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Continuous calculation of reliability and risk</h4>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Can be integrated with ERP, i.e. SAP</h4>
                </div>
            </div>
            <div className="flex flex-col items-center bg-line-grey relative">
                <div className="absolute right-0 bottom-0 z-20 " style={{ width: 256, height: 367 }}>
                    <Image src={abstract} />
                </div>
                <h2 className="text-4xl text-dark  my-7 ">Technology</h2>
                <div className="flex items-center w-full lg:mb-6 xl:mb-10 justify-center" >
                    <Image src={logo5} />
                    <div className="w-24 flex items-center justify-center">
                        <Image src={logo1} />
                    </div>
                    <Image src={logo2} />
                    <Image src={logo3} />
                    <div className="w-24 items-center flex justify-center">
                        <Image src={logo4} />
                    </div>
                </div>
            </div>
            <div className="flex items-center justify-center bg-darker-blue lg:py-11 w-full xl:py-20">
                <Link href={'/contact-us'}>
                    <button className="p-4 text-white bg-primary font-bold xl:text-xl lg:text-base">Contact Us</button>
                </Link>
            </div>
            <Footer />
        </div>
    )
}
