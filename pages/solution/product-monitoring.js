import Image from 'next/image'
import Footer from '../../component/layout/footer'
import Navbar from '../../component/layout/navbar'
import content1 from '/public/prod-mon-1.png'
import content2 from '/public/prod-mon-2.png'
import Link from 'next/link'
import { Carousel } from 'antd';
import logoTech1 from '/public/maintenance-logo1.png'
import logoTech2 from '/public/maintenance-logo2.png'
import logoTech3 from '/public/maintenance-logo5.png'
import logoTech4 from '/public/maintenance-logo4.png'

export default function ProductMonitoring() {
    return (
        <div>
            <div style={{
                backgroundImage: `url('/header-prod-mon.png')`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                height: 500
            }
            }
                className="relative w-full"
            >
                <Navbar />
                <div className="absolute left-7 bottom-12">
                    <h1 className="text-white space-x-2 space-y-8 m-0 font-robotoslab" style={{fontSize: 42}}>
                        Production & Monitoring Software
                    </h1>
                    <h2 className="text-white lg:text-sm xl:text-md font-sans " style={{ width: 500 }}>
                        Production output of a plant is the lifeblood of one's company. Our solution
                        enables our customers to achieve optimum plant production through real-time
                        and continuous plant production monitoring accessible through web and mobile application.
                        This level of accessibility allows the company to take preventive measures should a risk
                        of decreasing production arises.
                    </h2>
                </div>
            </div>
            <div className="grid grid-cols-2 pl-7 gap-7">
                <Carousel autoplay={true} >
                    <div>
                        <Image   src={content1} />
                    </div>
                    <div>
                        <Image   src={content2} />
                    </div>
                </Carousel>
                <div className="flex flex-col justify-center">
                    <h2 className="my-5 text-dark text-4xl">Features</h2>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Recording of plant production through manual entry or IOT</h4>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Automatic KPI calculation based on actual production and periodic production targets</h4>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Production forecast using AI and machine learning </h4>
                    <h4 className="text-light-grey mb-10 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Customizable production dashboard intelligence</h4>
                    <h2 className="font-bold text-dark text-2xl mb-4">Technology</h2>
                    <div className=" flex items-center">
                        <div>
                            <Image   src={logoTech3} />
                        </div>
                        <div className="flex w-24 justify-center items-center">
                            <Image   src={logoTech1} />
                        </div>
                        <div>
                            <Image   src={logoTech2} />
                        </div>
                        <div className="flex w-24 justify-center items-center">
                            <Image   src={logoTech4} />
                        </div>
                    </div>
                </div>
            </div>

            <div className="flex items-center justify-center bg-darker-blue lg:py-11 w-full xl:py-20">
                <Link href={'/contact-us'}>
                    <button className="p-4 text-white bg-primary font-bold xl:text-xl text-base">Contact Us</button>
                </Link>
            </div>
            <Footer />
        </div>
    )
}
