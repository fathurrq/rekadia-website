import Image from 'next/image'
import Footer from '../../component/layout/footer'
import Navbar from '../../component/layout/navbar'
import { Carousel } from 'antd';
import logo1 from '/public/maintenance-logo1.png'
import logo2 from '/public/maintenance-logo2.png'
import logo3 from '/public/maintenance-logo3.png'
import logo4 from '/public/maintenance-logo4.png'
import hsseCert1 from '/public/hsse-cert-1.png'
import hsseCert2 from '/public/hsse-cert-2.png'
import hsseCert3 from '/public/hsse-cert-3.png'
import hsseCert4 from '/public/hsse-cert-4.png'
import hsseMonitoring1 from '/public/hsse-monitoring-1.png'
import hsseMonitoring2 from '/public/hsse-monitoring-2.png'
import hsseIncident1 from '/public/hsse-incident-1.png'
import hsseIncident2 from '/public/hsse-incident-2.png'
import Link from 'next/link'

export default function Hsse() {
    return (
        <div>
            <div style={{
                backgroundImage: `url('/header-hsse.jpg')`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                height: 500,
            }
            }
                className="relative w-full"
            >
                <Navbar />
                <div className="absolute left-7 bottom-24">
                    <h1 className="text-white space-x-2 space-y-8 m-0  font-robotoslab " style={{ fontSize: 42 }}>
                        HSSE
                    </h1>
                    <h2 className="text-white lg:text-sm xl:text-base font-sans w-4/5 " style={{ width: 571 }}>
                        Conscious companies place HSSE (health, safety, security, and environment)
                        as the top priorities, emphasize the utmost importance of people and
                        environment in company's operation. Our software empowers companies to make it happen.
                    </h2>
                </div>
            </div>
            <div className="grid grid-cols-2 pl-7 gap-10 mt-20">
                <div>
                    <Carousel autoplay={true} style={{ color: "blue" }}>
                        <div>
                            <Image src={hsseIncident1} />
                        </div>
                        <div>
                            <Image src={hsseIncident2} />
                        </div>
                    </Carousel>
                </div>

                <div>
                    <h2 className="mb-5 mt-10 text-dark text-4xl">Incident Reporting Software</h2>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Recording of incidents and near miss across company operations</h4>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">RCA (root cause analysis) module to identify the root cause and solution in each incident </h4>
                    <h4 className="text-light-grey mb-36 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Proactively prevent future incidents through monitoring of action items execution</h4>
                    <h2 className="font-bold text-dark text-2xl mb-6">Technology</h2>
                    <div className="flex items-center justify-center" style={{ width: 100 }}>
                        <Image src={logo4} />
                    </div>
                </div>
            </div>
            <div className="grid grid-cols-2 px-12 pb-6 pt-9 gap-7 mt-20 bg-line-grey ">

                <div>
                    <h2 className="my-5 text-dark text-4xl">Environment Monitoring Software</h2>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Manual recording of environment quality (industrial waste content, air quality, and so on)</h4>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Real-time monitoring of environment quality using industrial sensor</h4>
                    <h4 className="text-light-grey mb-20 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Smart alert system should certain parameter have the potential to exceed industry standards</h4>
                    <h2 className="font-bold text-dark text-2xl mb-4">Technology</h2>
                    <div className=" flex items-center">
                        <div className="w-24 flex justify-center">
                            <Image src={logo1} />
                        </div>
                        <Image src={logo2} />
                        <Image src={logo3} />
                        <div className="w-24 flex justify-center">
                            <Image src={logo4} />
                        </div>
                    </div>
                </div>
                <div>
                    <Carousel autoplay={true} style={{ color: "blue" }}>
                        <div>
                            <Image src={hsseMonitoring1} />
                        </div>
                        <div>
                            <Image src={hsseMonitoring2} />
                        </div>
                    </Carousel>
                </div>
            </div>
            <div className="grid grid-cols-2 px-7 gap-7 my-20">
                <div>
                    <Carousel autoplay={true} style={{ color: "blue" }}>
                        <div>
                            <Image src={hsseCert1} />
                        </div>
                        <div>
                            <Image src={hsseCert2} />
                        </div>
                        <div>
                            <Image src={hsseCert3} />
                        </div>
                        <div>
                            <Image src={hsseCert4} />
                        </div>
                    </Carousel>
                </div>

                <div>
                    <h2 className="mb-5 text-dark text-4xl">Online HSSE Certification</h2>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Online course of HSSE subjects in e-book and video formats</h4>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Online test with dynamic multiple choice questions</h4>
                    <h4 className="text-light-grey mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Generation of e-certificate with certain validity period if user passed the test</h4>
                    <h4 className="text-light-grey mb-28 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium">Course assignment system based on organization, position, and department</h4>
                    <h2 className="font-bold text-dark text-2xl mb-4 ">Technology</h2>
                    <div className="w-24 flex justify-center">
                        <Image src={logo4} />
                    </div>
                </div>
            </div>
            <div className="flex items-center justify-center bg-darker-blue lg:py-11 w-full xl:py-20">
                <Link href={'/contact-us'}>
                    <button className="lg:px-8 lg:py-4 xl:px-12 xl:py-4 text-white bg-primary font-bold xl:text-xl">Contact Us</button>
                </Link>
            </div>
            <Footer />
        </div>
    )
}
