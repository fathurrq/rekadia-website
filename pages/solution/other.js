import Image from 'next/image'
import Footer from '../../component/layout/footer'
import Navbar from '../../component/layout/navbar'
import { Carousel } from 'antd';
import logo1 from '/public/maintenance-logo5.png'
import logo2 from '/public/maintenance-logo2.png'
import logo3 from '/public/maintenance-logo4.png'
import step1 from '/public/other-step-1.png'
import step2 from '/public/other-step-2.png'
import step3 from '/public/other-step-3.png'
import step4 from '/public/other-step-4.png'
import step5 from '/public/other-step-5.png'
import business1 from '/public/other-bussiness-int1.png'
import business2 from '/public/other-bussiness-int2.png'
import business3 from '/public/other-bussiness-int3.png'
import business4 from '/public/other-bussiness-int4.png'
import business5 from '/public/other-bussiness-int5.png'
import procure1 from '/public/other-e-procurement1.png'
import procure2 from '/public/other-e-procurement1.png'
import procure3 from '/public/other-e-procurement1.png'
import doc1 from '/public/other-doc-management1.png'
import doc2 from '/public/other-doc-management1.png'
import mobwork1 from '/public/other-mob-work1.png'
import mobwork2 from '/public/other-mob-work2.png'
import projectManagement1 from '/public/other-project-management1.png'
import projectManagement2 from '/public/other-project-management2.png'
import projectManagement3 from '/public/other-project-management3.png'
import Link from 'next/link'
import { useState } from 'react';

export default function Other() {
    const [menuActive, setMenuActive] = useState(0)
    return (
        <div>
            <div style={{
                backgroundImage: `url('/header-other.jpg')`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                height: 500,
            }
            }
                className="relative w-full"
            >
                <Navbar />
                <div className="absolute left-7 bottom-32">
                    <h1 className="text-whitespace-x-2 space-y-8 m-0  font-robotoslab text-white mb-1" style={{ fontSize: 42 }}>
                        Other Solution
                    </h1>
                    <h2 className="text-white text-sm font-sans " style={{ width: 640 }}>
                        We can help to develop software specific according to your needs.
                    </h2>
                </div>
            </div>
            <h2 className="text-dark lg:text-4xl xl:text-5xl mb-12 mt-8 mx-6">
                How it Works?
            </h2>
            <div className="grid grid-cols-5 px-6 mb-20">
                <div className="flex flex-col items-center">
                    <Image src={step1} />
                    <div className="mt-6 w-36" style={{ borderBottom: "3px solid #4ACFFF" }}>
                        <p className="text-sm text-dark text-center m-0 mb-4">Step 1</p>
                        <p className="text-lg text-dark text-center font-bold m-1 mb-4">Requirement Analysis</p>
                    </div>
                    <p className="text-sm text-dark text-center mt-4 w-40">
                        Define the expectations of the users
                        for a software that is to be built
                        or modified.
                    </p>
                </div>
                <div className="flex flex-col items-center">
                    <Image src={step2} />
                    <div className="mt-6 w-44" style={{ borderBottom: "3px solid #4ACFFF" }}>
                        <p className="text-sm text-dark text-center m-0 mb-4">Step 2</p>
                        <p className="text-lg text-dark text-center font-bold m-1 mb-4">Software Analysis and Design</p>
                    </div>
                    <p className="text-sm text-dark text-center mt-4 w-40">
                        Transform requirement into architectural
                        specification of software,
                        i.e. platform, database, prototype etc.
                    </p>
                </div>
                <div className="flex flex-col items-center">
                    <Image src={step3} />
                    <div className="mt-6 w-36" style={{ borderBottom: "3px solid #4ACFFF" }}>
                        <p className="text-sm text-dark text-center m-0 mb-4">Step 3</p>
                        <p className="text-lg text-dark text-center font-bold m-1 mb-4">Software Development</p>
                    </div>
                    <p className="text-sm text-dark text-center mt-4 w-40">
                        Write a series of interellated
                        programming code,
                        which provides the functionality
                        of the software.
                    </p>
                </div>
                <div className="flex flex-col items-center">
                    <Image src={step4} />
                    <div className="mt-6 w-36" style={{ borderBottom: "3px solid #4ACFFF" }}>
                        <p className="text-sm text-dark text-center m-0 mb-4">Step 4</p>
                        <p className="text-lg text-dark text-center font-bold m-1 mb-4">Software Testing</p>
                    </div>
                    <p className="text-sm text-dark text-center mt-4 w-40">
                        Check whether the software matches the expected results and ensure that the software is defect free.
                    </p>
                </div>
                <div className="flex flex-col items-center">
                    <Image src={step5} />
                    <div className="mt-6 w-36" style={{ borderBottom: "3px solid #4ACFFF" }}>
                        <p className="text-sm text-dark text-center m-0 mb-4">Step 5</p>
                        <p className="text-lg text-dark text-center font-bold m-1 mb-4">Software Implementation</p>
                    </div>
                    <p className="text-sm text-dark text-center mt-4 w-40">
                        Integrate the software into the workflow of the company.
                    </p>
                </div>
            </div>
            <div className="bg-line-grey px-7 gap-7 mt-20 pb-12 pt-24">
                <div className="grid grid-cols-2">
                    <div>
                        <h2 className="text-dark text-4xl mb-6">Custom Software Example</h2>
                        <h4 className={`${menuActive === 0 ? "text-light-grey" : "text-active-lighter"} mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium cursor-pointer `} onClick={() => setMenuActive(0)}>Business Intelligence Dashboard</h4>
                        <h4 className={`${menuActive === 1 ? "text-light-grey" : "text-active-lighter"} mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium cursor-pointer `} onClick={() => setMenuActive(1)}>E-Procurement Software</h4>
                        <h4 className={`${menuActive === 2 ? "text-light-grey" : "text-active-lighter"} mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium cursor-pointer `} onClick={() => setMenuActive(2)}>Document Management System</h4>
                        <h4 className={`${menuActive === 3 ? "text-light-grey" : "text-active-lighter"} mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium cursor-pointer `} onClick={() => setMenuActive(3)}>Mobile Workforce Management</h4>
                        <h4 className={`${menuActive === 4 ? "text-light-grey" : "text-active-lighter"} mb-7 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium cursor-pointer `} onClick={() => setMenuActive(4)}>Project Management Software</h4>

                        <h2 className="font-bold text-dark text-2xl mb-4">Technology</h2>
                        <div className="flex items-center justify-start">
                            <div className="flex w-24 items-center justify-center">
                                <Image src={menuActive === 3 ? logo3 : logo1} />
                            </div>
                            {
                                (menuActive === 0 || menuActive === 4) &&
                                <>
                                    <div className="flex w-24 items-center justify-center">
                                        <Image src={logo2} />
                                    </div>
                                    <div className="flex w-24 items-center justify-center">
                                        <Image src={logo3} />
                                    </div>
                                </>
                            }
                        </div>
                    </div>
                    <div>
                        {
                            menuActive === 0 &&
                            <Carousel autoplay={true} style={{ color: "blue" }}>
                                <div>
                                    <Image src={business1} />
                                </div>
                                <div>
                                    <Image src={business2} />
                                </div>
                                <div>
                                    <Image src={business3} />
                                </div>
                                <div>
                                    <Image src={business4} />
                                </div>
                                <div>
                                    <Image src={business5} />
                                </div>
                            </Carousel>
                        }
                        {
                            menuActive === 1 &&
                            <Carousel autoplay={true} style={{ color: "blue" }}>
                                <div>
                                    <Image src={procure1} />
                                </div>
                                <div>
                                    <Image src={procure2} />
                                </div>
                                <div>
                                    <Image src={procure3} />
                                </div>
                            </Carousel>
                        }
                        {
                            menuActive === 2 &&
                            <Carousel autoplay={true} style={{ color: "blue" }}>
                                <div>
                                    <Image src={doc1} />
                                </div>
                                <div>
                                    <Image src={doc2} />
                                </div>
                            </Carousel>
                        }
                        {
                            menuActive === 3 &&
                            <Carousel autoplay={true} style={{ color: "blue" }}>
                                <div>
                                    <Image src={mobwork1} />
                                </div>
                                <div>
                                    <Image src={mobwork2} />
                                </div>
                            </Carousel>
                        }
                        {
                            menuActive === 4 &&
                            <Carousel autoplay={true} style={{ color: "blue" }}>
                                <div>
                                    <Image src={projectManagement1} />
                                </div>
                                <div>
                                    <Image src={projectManagement2} />
                                </div>
                                <div>
                                    <Image src={projectManagement3} />
                                </div>
                            </Carousel>
                        }
                    </div>
                </div>
            </div>
            <div className="flex items-center justify-center bg-darker-blue lg:py-11 w-full xl:py-20">
                <Link href={'/contact-us'}>
                    <button className="lg:px-8 lg:py-4 xl:px-12 xl:py-4 text-white bg-primary font-bold xl:text-xl">Contact Us</button>
                </Link>
            </div>
            <Footer />
        </div>
    )
}
