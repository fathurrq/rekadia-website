import Image from 'next/image'
import Footer from '../component/layout/footer'
import Navbar from '../component/layout/navbar'
import map from '/public/map.png'
import Link from 'next/link'
import pin from '/public/pin-white.svg'
import phone from '/public/wa-white.png'
import email from '/public/mail-white.png'
import { useState } from 'react'


export default function ContactUs() {
    const [name, setName] = useState('')
    const [emailForm, setEmailForm] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const [subject, setSubject] = useState('')
    const [message, setMessage] = useState('')

    return (
        <div>
            <div className="relative">
                <div style={{
                    backgroundImage: `url('/header-contact.png')`,
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "cover",
                    height: 900
                }
                }
                    className="relative w-full"

                >
                    <Navbar />
                </div>
                <div className="absolute flex top-48 flex-col w-full items-center">
                    <h2 className=" text-white mb-3 font-robotoslab" style={{ fontSize: 42 }}>Feel Free to Contact Us</h2>
                    <input onChange={(e) => setName(e.target.value)} className="rounded p-3 pl-2 shadow-inner mb-5 xl:mt-0" style={{ width: 600, height: 42, boxShadow: 'inset 0px 1px 3px rgba(0, 0, 0, 0.5)' }} placeholder="Name*" />
                    <input onChange={(e) => setEmailForm(e.target.value)} className="rounded p-3 pl-2 shadow-inner mb-5" style={{ width: 600, height: 42, boxShadow: 'inset 0px 1px 3px rgba(0, 0, 0, 0.5)' }} placeholder="Email*" />
                    <input onChange={(e) => setPhoneNumber(e.target.value)} className="rounded p-3 pl-2 shadow-inner mb-5" style={{ width: 600, height: 42, boxShadow: 'inset 0px 1px 3px rgba(0, 0, 0, 0.5)' }} placeholder="Phone Number" />
                    <input onChange={(e) => setSubject(e.target.value)} className="rounded p-3 pl-2 shadow-inner mb-5" style={{ width: 600, height: 42, boxShadow: 'inset 0px 1px 3px rgba(0, 0, 0, 0.5)' }} placeholder="Subject*" />
                    <input onChange={(e) => setMessage(e.target.value)} className="rounded pl-2 shadow-inner mb-5 pt-3 pb-64" style={{ width: 600, height: 240, boxShadow: 'inset 0px 1px 3px rgba(0, 0, 0, 0.5)' }} placeholder="Your Message" />
                    <div className="flex justify-between" style={{ width: 600 }}>
                        <p className="lg:text-xs xl:text-sm text-line-grey">* must filled</p>

                    </div>
                    <div className="flex justify-end" style={{ width: 600 }}>

                        <button className="lg:px-6 lg:py-3 xl:px-8 xl:py-4 text-white bg-primary font-bold xl:text-xl">Contact Us</button>
                    </div>
                </div>
            </div>
            <div className="w-full bg-darker-blue grid grid-cols-2">
                <div className="p-5 flex">
                    <div className="w-full pl-3">
                        <Image src={map} />
                    </div>
                </div>
                <div className="grid grid-cols-2 py-4">
                    <div>
                        <h2 className="text-2xl text-white xl:mb-10 lg:mb-4">Location</h2>
                        <div className="flex pt-1 items-center">
                            <div className="flex items-center mr-6">
                            <Image src={pin} />
                            </div>
                            <h2 className="text-white font-bold text-lg m-0">Jakarta</h2>
                        </div>
                        <div className="flex flex-col xl:mb-3 lg:mb-0 ml-14">
                            <h2 className="text-white text-sm"> Metropolitan Tower, Lv. 13 <br/> Jl. R.A. Kartini Kav. 14, TB <br/>Simatupang, Jakarta Selatan</h2>
                        </div>
                        <div className="flex pt-1 items-center">
                            <div className="flex items-center mr-6">
                            <Image src={pin} />
                            </div>
                            <h2 className="text-white font-bold text-lg m-0">Bandung</h2>
                        </div>
                        <div className="flex flex-col xl:mb-3 lg:mb-0 ml-14">
                            <h2 className="text-white text-sm w-48 m-0 p-0">
                                Jl. Pasir Salam Raya I/9, BANDUNG, Indonesia
                            </h2>
                        </div>
                    </div>
                    <div className="border-l pl-5">
                        <h2 className="text-2xl text-white mb-10">Contact</h2>
                        <div className="flex mb-2">
                            <div>
                                <Image src={phone} height={18} width={18} />
                            </div>
                            <h2 className="text-white text-xs ml-2 mb-4">(021) 2955 2653</h2>
                        </div>
                        <div className="flex mb-4">
                            <div>
                                <Image src={email} height={15} width={19} />
                            </div>
                            <h2 className="text-white text-xs ml-2">contact@rekadia.co.id</h2>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}
