import Image from 'next/image'
import content1 from '/public/about-us-1.png'
import content2 from '/public/about-us-2.png'
import content3 from '/public/about-us-3.jpg'
import content4 from '/public/about-us-4.jpg'
import content5 from '/public/about-us-5.jpg'
import Footer from '../component/layout/footer'
import Navbar from '../component/layout/navbar'
import Link from 'next/link'

export default function AboutUs() {
    return (
        <div>
            <div style={{
                backgroundImage: `url('/header-about-us.jpg')`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
            }
            }
                className="relative h-header w-full"
            >
             <Navbar/>
                <div className="absolute left-7 lg:bottom-52">
                    {/* <h2 className="text-white lg:text-xl xl:text-2xl mb-6 font-sans">We Are Here for Your Business</h2> */}
                    <h1 className="text-white space-x-2 space-y-8 font-robotoslab  p-0 m-0" style={{fontSize: 42}}>
                        A Partner You Can Trust
                    </h1>
                    <p className="text-white text-sm lg:w-3/5 xl:w-2/5 ">
                        PT Rekadia Solusi Teknologi is an integrated digital
                        transformation partner that you can trust. Our mission
                        is to ensure the success of our customer's digital
                        transformation through technology, methodology,
                        and people. Many of our long-time customers rely
                        upon us for years since we always keep their
                        interest at heart and we stay true to our commitment.
                    </p>
                </div>
            </div>
            <div className="grid grid-cols-2 px-7 py-10 gap-8">
                <div>
                    <Image   src={content1} />
                </div>
                <div className="flex flex-col justify-center">
                    <div>
                        <h2 className="text-dark text-2xl mb-4">Our Specialities</h2>
                        <p className=" text-light-grey text-sm" style={{width: 472}}>Our specialties are digital solutions for management
                            and business operation through web, mobile application,
                            and IOT (Internet of Things). We create business value by
                            increasing operational efficiency, facilitating the access
                            to the information and assisting in management decision-making.</p>
                    </div>
                    <div>
                        <p className=" text-light-grey text-sm" style={{width: 472}}>We serve many leading companies in Indonesia,
                            especially in oil & gas, petrochemical and geothermal sectors.These sectors are unique
                            since involving complex processes and heavily regulated by the government. With more than
                            10 years of experience in these sectors, we deeply understand the business and challenges
                            faced by our customers.</p>
                    </div>
                </div>
            </div>
            <div className="grid grid-cols-2">
                <div className="gradient-bg flex flex-col items-center justify-center">
                    <p className=" text-white text-lg font-bold text-center mb-12 xl:text-2xl">Our Mission</p>
                    <p className="text-white text-4xl text-center" style={{ width: 500 }}>Provides effective solutions to empower organizations</p>
                </div>
                <div className="bg-light-green flex items-center justify-center p-4" style={{ height: "440px" }}>
                    <Image   src={content2} />
                </div>
            </div>
            <h2 className="text-center mt-12 text-4xl text-dark mb-11">Why Work With Us</h2>
            <div className="grid grid-cols-2 px-7">
                <div className="flex justify-center items-center">
                    <Image   src={content3} />
                </div>
                <div className="flex items-center justify-center">
                    <p className="lg:text-sm xl:text-md text-light-grey leading-4 px-16">We are committed to provide superior services and pay
                        close attention to the details to provide the best results
                        for our customers. We are referring to proven standards and
                        methodologies in the IT industry, and then combine them with
                        our vast experience in real-world applications.</p>
                </div>
            </div>
            <div className="grid grid-cols-2 px-7">
                <div className="flex items-center">
                    <p className="lg:text-sm xl:text-md text-light-grey leading-4 px-16">
                        After implementation, IT system often becomes a backbone
                        for company operation. We make customers feel secure with
                        the stability of business operation through warranty, service
                        level agreement, and responsive after sale service support.
                    </p>
                </div>
                <div className="flex justify-center items-center">
                    <Image   src={content4} />

                </div>
            </div>
            <div className="grid grid-cols-2 px-7 mb-12">
                <div className="flex justify-center items-center">
                    <Image   src={content5} />
                </div>
                <div className="flex items-center">
                    <p className="lg:text-sm xl:text-md text-light-grey leading-4 px-16">We believe that trust is the most important thing
                        and the foundation of every business relationship.
                        Thus, we explain in detail the work process and the
                        basis calculation of IT investment to customers.</p>
                </div>
            </div>
            <div className="w-full h-64 bg-darker-blue flex flex-col items-center justify-center">
                <p className=" text-white font-robotoslab text-center leading-tight mb-6" style={{fontSize: 42, width:400}}>Yes, We Are Always Ready to Help You</p>
                <Link href={'/contact-us'}>
                    <button className="px-12 py-3 text-white bg-primary font-bold text-base">Contact Us</button>
                </Link>

            </div>

            <Footer />
        </div>
    )
}
