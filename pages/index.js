import Image from 'next/image'
import { HiArrowRight } from 'react-icons/hi'
import computerImage from '/public/home-image-1.png'
import oilImage from '/public/home-image-2.png'
import imageMac from '/public/image_mockup.png'
import map from '/public/map.png'
import Slider from '../component/HorizontalSlider'
import abstract from '/public/abstract-motive.png'
import Navbar from '../component/layout/navbar'
import Footer from '../component/layout/footer'
import logo1 from '/public/technology/logoo1.png'
import logo2 from '/public/technology/logoo2.png'
import logo3 from '/public/technology/logoo3.png'
import logo4 from '/public/technology/logoo4.png'
import logo5 from '/public/technology/logoo5.png'
import logo6 from '/public/technology/logoo6.png'
import logo7 from '/public/technology/logoo7.png'
import Link from 'next/link'
import { useEffect, useState } from 'react'

const menu = [
  {
    key: 0,
    title: "Web & Mobile Application",
    content: "Empower business operation and management through web & mobile application enabling single source of truth and centralized data",
    link: '/'
  },
  {
    key: 1,
    title: "Data Integration",
    content: "Seamlessly integrate our application with ERP and other 3rd party software",
    link: '/'
  },
  {
    key: 2,
    title: "System Integration",
    content: "Enabling data integration and data transfer between multiple application",
    link: '/'
  },
  {
    key: 3,
    title: "IoT",
    content: "Gain instant insight through real-time data information from DCS and accurate analytics",
    link: '/'
  }
]
const Home = () => {
  const [menuActive, setMenuActive] = useState(0)
  const [bigScreen, setBigScreen] = useState(false)
  const [isMobile, setIsMobile] = useState(false)

  useEffect(() => {
    if (window.innerWidth > 1190) {
      setBigScreen(true)
      console.log(window.innerWidth)
      console.log(bigScreen)
    }
    else if (window.innerWidth < 500) {
      setIsMobile(true)
    }
  }, [])
  return (
    <div className="w-full flex flex-col">
      <div style={isMobile ? { height: 300 } : { height: 600 }} className="w-full">
        <video className="xl:relative lg:relative sm:static w-full" height="auto"
          poster="" muted autoplay loop preload autoPlay
        >
          <source src='/home-footage.mp4' type="video/mp4" />

        </video>
      </div>
      <Navbar />
      <div className="xl:absolute xl:left-7 xl:z-30 lg:absolute lg:left-7 lg:z-30 sm:static " style={bigScreen ? { top: 256 } : { top: 200 }}>
        <h2 className="text-white lg:text-2xl xl:text-2xl mb-6 font-sans">We Are Here for Your Business</h2>
        <h1 className="text-white lg:w-4/5 xl:w-3/5 space-x-2 space-y-8 tracking-wider leading-tight mb-6 font-robotoslab" style={{ fontSize: 42 }}>
          Gain competitive advantage through digital transformation in business operation and management
        </h1>
        <Link href={'/contact-us'}>
          <button className=" p-4 text-white bg-primary font-bold text-base font-helvetica">Contact Us</button>
        </Link>
      </div>
      <div className="w-full flex bg-darker justify-center relative flex-col" style={bigScreen ? undefined : { marginTop: -60 }}>
        <h1 className="text-white text-lg font-bold mb-2.5 pb-2.5 mt-8 ml-16  my-2.5 py-2.5 pl-4 pr-16 ">Services</h1>
        <div className="w-full flex xl:flex-row lg:flex-row sm:flex-col">
          <div className="flex flex-col items-start mx-16 pb-8">
            {
              menu.map((d) => {
                return <button onClick={() => setMenuActive(d.key)} className={(menuActive === d.key) ? `w-full text-left text-white text-2xl bg-primary p-4 font-bold` : `w-full text-left text-primary text-2xl p-4 font-bold hover:text-white hover:bg-primary`}>{d.title}</button>
              })
            }
          </div>
          <div className="flex flex-col xl:w-96 lg:w-96 sm:w-80">
            <p className="w-80 text-white lg:text-sm xl:text-lg">{menu[menuActive].content}</p>
            <a className="flex text-primary items-center font-bold mt-5 justify-end">See More <HiArrowRight className=" text-primary ml-4 w-8 h-8" /> </a>
          </div>
        </div>
        <div className="absolute right-0 xl:block lg:block sm:hidden " style={{ bottom: "-55px" }}>
          <Image src={computerImage} />
        </div>
      </div>
      <div className="flex flex-col w-full">
        <div className="flex pt-14 ml-20 xl:justify-center">
          <h1 className="text-4xl text-dark lg:mr-44" >Technology <br /> Expertise</h1>
          <div class="grid grid-cols-4 mb-9 gap-1">
            <div className="flex justify-center items-center p-2">
              <Image src={logo1} />
            </div>
            <div className="flex justify-center items-center p-2">
              <Image src={logo2} />
            </div>
            <div className="flex justify-center items-center p-2">
              <Image src={logo3} />
            </div>
            <div className="flex items-center p-2">
              <Image src={logo4} />
            </div>
            <div className="flex justify-center items-center p-2">
              <Image src={logo5} />
            </div>
            <div className="flex justify-center items-center p-2">
              <Image src={logo6} />
            </div>
            <div className="flex justify-center items-center p-2">
              <Image src={logo7} />
            </div>
          </div>
        </div>
        <div className="w-full px-6 relative">
          <div className="absolute left-0 z-50 xl:block lg:block sm:hidden" style={{ bottom: -20 }}>
            <Image src={oilImage} width={469} height={407} />
          </div>
          <div className=" w-full h-80 grid grid-cols-2 gap-8 gradient-bg">
            <div></div>
            <div className="lg:mr-8">
              <h1 className="text-white lg:text-4xl mb-10 xl:text-5xl mt-14">Our Speciality</h1>
              <h1 className="text-white lg:text-lg xl:text-2xl mt-11 font-bold">
                We have specialty in oil & gas, petrochemical and
                geothermal sector. With more than 10 years of experience
                in the sector, you will have a proven partner deeply understand your needs and challenges.</h1>
            </div>
          </div>
        </div>
      </div>
      <div className="relative pt-11 px-0 bg-line-grey sm:flex sm:flex-col " style={{ height: "465px" }}>
        <div className="absolute right-0 z-10 lg:top-64 xl:top-56" style={{ width: 256, height: 367 }}>
          <Image src={abstract} />
        </div>
        <div className="xl:absolute lg:absolute sm:static l-0" style={{ width: "50%" }}>
          <Image src={imageMac} />
        </div>
        <div className="w-full grid grid-cols-2 lg:gap-0 xl:gap-0" style={{ height: "450px" }}>
          <div className="sm:hidden xl:block lg:block"></div>
          <div className="flex flex-col p-4" style={{ width: "450px" }}>
            <h2 className="text-light-grey font-bold mb-2 lg:text-lg xl:text-xl">Featured Solution</h2>
            <h2 className="text-dark font-bold lg:text-2xl xl:text-3xl">Maintenance & Inspection Software</h2>
            <h2 className="text-darker-blue mb-5 lg:text-sm xl:text-lg">Maintenance software aims to improve plant productivity by maximizing
              plant uptime and reducing unplanned downtime. Our software enables company
              to proactively ensuring the optimal operating condition of equipment through
              comprehensive maintenance schedule, recording of maintenance activities,
              and monitoring.</h2>
            <Link href={'/solution/maintenance'}>
              <a className="w-full flex items-center font-bold text-primary justify-end">
                <h2 className="text-primary font-bold">See More</h2>
                <HiArrowRight className=" text-primary ml-4 w-8 h-8" />
              </a>
            </Link>
          </div>

        </div>
      </div>
      <div className="flex flex-col justify-center items-center lg:px-12 xl:px-48">
        <h2 className="font-bold xl:text-3xl lg:text-2xl mt-28 text-dark">Who's Already Trusting Us</h2>
        <div className="w-full">
          <Slider />
        </div>
      </div>
      <div className="lg:h-16 xl:h-24" />
      <div className="w-full bg-darker-blue grid grid-cols-2">
        <div className="flex justify-center items-center flex-col">
          <h2 className="text-white leading-tight w-80 text-center mb-6 font-robotoslab" style={{ fontSize: 42 }}>Learn How We
            Can Help You</h2>
          <Link href={'/contact-us'}>
            <button className="lg:px-8 lg:py-2 xl:px-12 xl:py-4 text-white bg-primary font-bold xl:text-xl">Contact Us</button>
          </Link>
        </div>
        <div className="flex justify-center items-center p-4">
          <div className="w-full">
            <Image src={map} />
          </div>
        </div>
      </div>
      <Footer />
    </div>
  )
}

export default Home